#!/bin/bash

declare -A hash_to_site

add_file() {
    local pass_hash="$($GPG -d "${GPG_OPTS[@]}" "$1" | head -n 1 | tr -d '\n' | sha1sum | cut -d" " -f 1)"
    local site="${1#${PREFIX}/}"
    site="${site#./}"
    site="${site%.gpg}"
    local old_site="${hash_to_site[$pass_hash]}"
    hash_to_site["$pass_hash"]="${old_site}${old_site:+ }${site}"
}

if [[ $# == 0 ]]; then
    set -- .
fi

while [[ $# != 0 ]]; do
    local path="$1"
    check_sneaky_paths "$path"
    local passfile="$PREFIX/$path.gpg"
    local passdir="$PREFIX/$path"

    if [[ -f "$passfile" ]]; then
        add_file "$passfile"
    elif [[ -d "$passdir" ]]; then
        local file
        for file in $(find "$passdir" -type f -name "*.gpg"); do
            add_file "$file"
        done
    else
        die "$1 is not a file or directory"
    fi
    shift
done


cat <<EOF
create temporary table pass_hashes (
    hash bytea
        not null
        primary key,
    sites text
);

insert into pass_hashes
values
EOF

local hash
local first=true

for hash in "${!hash_to_site[@]}"; do
    if [[ "$first" == "false" ]]; then
        printf ",\n"
    fi
    first="false"
    echo -n "    ('\x${hash}'::bytea, '${hash_to_site[$hash]}')"
done

cat <<EOF

;

select sites
from pass_hashes
where hash in (select hash from hashes);
EOF
