Have my password-store been pwned?
==================================

This set of scripts allows to import data from
[https://haveibeenpwned.com/Passwords](https://haveibeenpwned.com/Passwords)
into a postgresql database and check your password-store if any of
your passwords have been pwned.

The code is released under the terms of WTFPL.

Required disk space
-------------------

Decompressed pwned-passwords-2.0.txt file weights about 30GB.

Database files after importing hashes weight about 50GB.

Importing data into postgresql
------------------------------

1.  Unpack hashes into a file `pwned-passwords-2.0.txt`.
2.  Create `db_data` directory.
3.  Start the database using `docker-compose up -d`. This will start
    importing the data.
4.  Have a break. This will take some time.
5.  Once importing is complete, you can delete `db.sql` and
    `pwned-passwords.txt` from volumes in `docker-compose.yml` file.


Extracting hashes from password-store
-------------------------------------

1.  Install `pwned.bash` as a password-store plugin.
2.  Run `pass pwned >pass.sql`. You can optionally narrow checking to
    specified files or directories.
3.  At this point you can inspect `pass.sql` to see if you have the
    same password on multiple websites. They will be space separated
    next to a hash.


Checking extracted hashes with the database
-------------------------------------------

1.  Run `./psql.sh -f /data/pass.sql`. The output will show you
    websites which password hashes were found in the database.
