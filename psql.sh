#!/bin/bash

exec docker run \
    -it \
    --rm \
    --network=pwndb_default \
    -e PGPASSWORD="u7QG80tKXoTa6Prn" \
    -v ${PWD}:/data:ro \
    postgres:alpine \
    psql -h pwndb_db_1 -U postgres "$@"
