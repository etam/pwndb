create temporary table tmp (
    hash char(40)
        not null,
    cnt int
        not null
);

copy tmp
from '/pwned-passwords-2.0.txt'
with
    delimiter ':'
;


create table hashes (
    hash bytea
        not null
        primary key
);

insert into hashes (hash)
select (('\x'||hash)::bytea)
from tmp;

drop table tmp;
